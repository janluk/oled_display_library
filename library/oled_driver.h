/*
 * oled_driver.h
 *
 *  Created on: Nov 3, 2023
 *      Author: Jan Lukaszewicz
 */

#ifndef oled_DRIVER_H_
#define oled_DRIVER_H_

struct oled_driver
{
	void (*WriteOneByte)(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t ByteToSend);
	void (*WriteBurst)(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t *DataToSend, uint16_t SizeDataToSend);

};

#endif /* oled_DRIVER_H_ */
