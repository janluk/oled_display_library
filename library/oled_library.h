/*
 * oled_library.h
 *
 *  Created on: Nov 4, 2023
 *      Author: Jan Lukaszewicz
 */

#ifndef OLED_LIBRARY_H_
#define OLED_LIBRARY_H_

#include "i2c.h"
#include "stdint.h"

#define OLED_I2C_TIMEOUT 1000
#define OLED_COMMAND_ADDRESS 0x00
#define OLED_DATA_ADDRESS	0x40

#define SSD1306_128_64				1
#define SH1106_128_64				4
#define OLED_128_64_LCDWIDTH    128
#define OLED_128_64_LCDHEIGHT   64

#define SSD1306_128_32				2
#define SH1106_128_32				5
#define OLED_128_32_LCDWIDTH    128
#define OLED_128_32_LCDHEIGHT   32

#define SSD1306_96_16				3
#define SH1106_96_16				6
#define OLED_96_16_LCDWIDTH    	96
#define OLED_96_16_LCDHEIGHT    16

#define OLED_BLACK 0
#define OLED_WHITE 1
#define OLED_INVERSE 2

#define OLED_SETCONTRAST 0x81
#define OLED_DISPLAYALLON_RESUME 0xA4
#define OLED_DISPLAYALLON 0xA5
#define OLED_NORMALDISPLAY 0xA6
#define OLED_INVERTDISPLAY 0xA7
#define OLED_DISPLAYOFF 0xAE
#define OLED_DISPLAYON 0xAF

#define OLED_SETDISPLAYOFFSET 0xD3
#define OLED_SETCOMPINS 0xDA

#define OLED_SETVCOMDETECT 0xDB

#define OLED_SETDISPLAYCLOCKDIV 0xD5
#define OLED_SETPRECHARGE 0xD9

#define OLED_SETMULTIPLEX 0xA8


#define SH1106_SETLOWCOLUMN 0x02
#define SSD1306_SETLOWCOLUMN 0x00

#define OLED_SETHIGHCOLUMN 0x10
#define OLED_SETSTARTLINE 0x40

#define OLED_MEMORYMODE 0x20
#define OLED_COLUMNADDR 0x21
#define OLED_PAGEADDR   0x22

#define OLED_COMSCANINC 0xC0
#define OLED_COMSCANDEC 0xC8

#define OLED_SEGREMAP 0xA0

#define OLED_CHARGEPUMP 0x8D

#define OLED_EXTERNALVCC 0x1
#define OLED_SWITCHCAPVCC 0x2

// Scrolling #defines
#define OLED_ACTIVATE_SCROLL 0x2F
#define OLED_DEACTIVATE_SCROLL 0x2E

//
// oled_StartScrollDiag
//
#define OLED_SET_VERTICAL_SCROLL_AREA 0xA3

#define OLED_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define OLED_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A

//
// oled_StartScroll
//
#define OLED_RIGHT_HORIZONTAL_SCROLL 0x26
#define OLED_LEFT_HORIZONTAL_SCROLL 0x27



/**
 * @ingroup oled
 *   Error state
 */
typedef enum
{
	oled_NoError = 0, 			///< 0 No Errors
	oled_HAL_ERROR,				///< 1 HAL Errors
	oled_HAL_BUSY,				///< 2 HAL Transmision is Busy
	oled_HAL_TIMEOUT,				///< 3 HAL Transmision timeout
	oled_ErrorWriteToRegister,	///< 4 Registry write error
	oled_ErrorChipID,				///< 5 Wrong or damaged BMP sensor
	oled_ErrorLinkDriver,			///< 6 Driver link error
	oled_ErrorCallbackPointer,	///< 7 Callback pointer error
	oled_ErrorRead,				///< 8 Read from registry BMP error
	oled_ErrorWrite,				///< 9 Write to registry BMP error
	oled_ErrorIncorrectPosition,	///< 10 incorrect x or y position
	oled_ErrorBadOledType,		///< 11 incorrect oled type
}oled_status_t;

typedef struct oled_driver oled_driver_t;

typedef struct{
	uint16_t buffer_size;
	uint8_t *buffer;

	uint8_t typeOled;
	uint8_t widthOled;
	uint8_t heightOled;

	uint8_t i2cOledAdreess;
	I2C_HandleTypeDef *i2cOledHandle;

	oled_driver_t *driver;
}oled_t;

#include "oled_driver.h"
oled_status_t oled_Init(oled_t *oled, uint8_t OledType, I2C_HandleTypeDef *OledI2cHandle, uint8_t OledAdress);
oled_status_t Oled_DeInit(oled_t *oled, uint8_t OledType, I2C_HandleTypeDef *OledI2cHandle, uint8_t OledAdress);

oled_status_t oled_Display(oled_t *oled);
void oled_ClearBuffer(oled_t *oled, uint8_t PixelColor);
oled_status_t oled_DrawPixel(oled_t *oled, int16_t x, int16_t y, uint8_t Color);

void oled_StopScroll(oled_t *oled);
void oled_StartScrollDiagLeft(oled_t *oled, uint8_t start, uint8_t stop);
void oled_StartScrollDiagRight(oled_t *oled, uint8_t start, uint8_t stop);
void oled_StartScrollLeft(oled_t *oled, uint8_t Start, uint8_t Stop);
void oled_StartScrollRight(oled_t *oled, uint8_t Start, uint8_t Stop);
void oled_invertDisplay(oled_t *oled, uint8_t InvertON);

oled_status_t oled_UnLinkDriver(oled_t *oled);
oled_status_t oled_LinkDriver(oled_t *oled, oled_driver_t *driver_oled);

#endif /* OLED_LIBRARY_H_ */
