/*
 * oled_i2c_interface.c
 *
 *  Created on: Nov 3, 2023
 *      Author: Jan Lukaszewicz
 */

#include "oled_i2c_interface.h"

static oled_driver_t oledDriver ={
		oled_WriteOneByte,
		oled_WriteBurst,
};

oled_driver_t* oled_GetI2cDriver(void)
{
	return &oledDriver;
}

void oled_WriteOneByte(oled_t *oled,  uint8_t CommandOrDataAddress, uint8_t CommandToSend)
{
	HAL_I2C_Mem_Write(oled->i2cOledHandle, ((oled->i2cOledAdreess)<<1), CommandOrDataAddress, 1, &CommandToSend, 1, OLED_I2C_TIMEOUT);
}

void oled_WriteBurst(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t *CommandToSend, uint16_t SizeDataToSend)
{
	HAL_I2C_Mem_Write(oled->i2cOledHandle, ((oled->i2cOledAdreess)<<1), CommandOrDataAddress, 1, CommandToSend, SizeDataToSend, OLED_I2C_TIMEOUT);
}

