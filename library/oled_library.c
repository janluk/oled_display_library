/*
 * oled_library.c
 *
 *  Created on: Nov 4, 2023
 *      Author: Jan Lukaszewicz
 */
#include "oled_library.h"
#include "string.h"
#include "stdlib.h"

//
// driver
//
oled_status_t oled_LinkDriver(oled_t *oled, oled_driver_t *driver_oled)
{
	if (NULL == driver_oled)
	{
		return oled_ErrorLinkDriver;
	}

	oled->driver=driver_oled;

	return oled_NoError;
}

oled_status_t oled_UnLinkDriver(oled_t *oled)
{
	oled->driver = NULL;
	return oled_NoError;
}

//
// additional functions
//
void oled_invertDisplay(oled_t *oled, uint8_t InvertON)
{
  if (InvertON)
  {
	  oled->driver->WriteOneByte(oled, OLED_COMMAND_ADDRESS, OLED_INVERTDISPLAY);

  } else
  {
	  oled->driver->WriteOneByte(oled, OLED_COMMAND_ADDRESS, OLED_NORMALDISPLAY);
  }
}

// oled_startscrollright
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// oled_StartScrollRight(&oled1, 0x00, 0x0F);
void oled_StartScrollRight(oled_t *oled, uint8_t Start, uint8_t Stop)
{
	uint8_t CommandToSend[]=
	{
			OLED_RIGHT_HORIZONTAL_SCROLL,
			0X00,
			Start,
			0X00,
			Stop,
			0X00,
			0XFF,
			OLED_ACTIVATE_SCROLL,
	};

	oled->driver->WriteBurst(oled, OLED_COMMAND_ADDRESS, CommandToSend, sizeof(CommandToSend));
}

// oled_StartScrollLeft
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// oled_StartScrollLeft(&oled1, 0x00, 0x0F)
void oled_StartScrollLeft(oled_t *oled, uint8_t Start, uint8_t Stop)
{
	uint8_t CommandToSend[]=
	{
			OLED_LEFT_HORIZONTAL_SCROLL,
			0X00,
			Start,
			0X00,
			Stop,
			0X00,
			0XFF,
			OLED_ACTIVATE_SCROLL,
	};

	oled->driver->WriteBurst(oled, OLED_COMMAND_ADDRESS, CommandToSend, sizeof(CommandToSend));
}

// oled_StartScrollDiagRight
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// oled_StartScrollDiagRight(&oled1, 0x00, 0x0F)
void oled_StartScrollDiagRight(oled_t *oled, uint8_t start, uint8_t stop)
{
	uint8_t CommandToSend[]=
	{
			OLED_SET_VERTICAL_SCROLL_AREA,
			0X00,
			oled->heightOled,

			OLED_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL,
			0X00,
			start,
			0X00,
			stop,
			0X01,
			OLED_ACTIVATE_SCROLL,
	};

	oled->driver->WriteBurst(oled, OLED_COMMAND_ADDRESS, CommandToSend, sizeof(CommandToSend));
}

// oled_StartScrollDiagLeft
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// oled_StartScrollDiagLeft(&oled1, 0x00, 0x0F)
void oled_StartScrollDiagLeft(oled_t *oled, uint8_t start, uint8_t stop)
{
	uint8_t CommandToSend[]=
	{
			OLED_SET_VERTICAL_SCROLL_AREA,
			0X00,
			oled->heightOled,
			OLED_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL,
			0X00,
			start,
			0X00,
			stop,
			0X01,
			OLED_ACTIVATE_SCROLL,
	};

	oled->driver->WriteBurst(oled, OLED_COMMAND_ADDRESS, CommandToSend, sizeof(CommandToSend));
}

void oled_StopScroll(oled_t *oled)
{
	oled->driver->WriteOneByte(oled, OLED_COMMAND_ADDRESS, OLED_DEACTIVATE_SCROLL);
}

//
// main functions
//
oled_status_t oled_DrawPixel(oled_t *oled, int16_t x, int16_t y, uint8_t Color)
{
//	#define OLED_SELECT_BYTE ((x) + ((y) / 8) * (oled->Oled_width))
	#define OLED_SET_BIT ((1 << ((y) & 7)))

	if((x < 0) || (x >= oled->widthOled) || (y < 0) || (y >= oled->heightOled))
	{
		return oled_ErrorIncorrectPosition;
	}

	switch (Color)
	{
		case OLED_WHITE:
	 		oled->buffer[((x) + ((y) / 8) * (oled->widthOled))] |= OLED_SET_BIT;
	 		break;
	 	case OLED_BLACK:
	 		oled->buffer[((x) + ((y) / 8) * (oled->widthOled))] &= ~OLED_SET_BIT;
	 		break;
	 	case OLED_INVERSE:
	 		oled->buffer[((x) + ((y) / 8) * (oled->widthOled))] ^= OLED_SET_BIT;
	 		break;
	 }

	return oled_NoError;
}

void oled_ClearBuffer(oled_t *oled, uint8_t PixelColor)
{
	switch(PixelColor)
	{
	case OLED_WHITE:
		memset(oled->buffer, 0xFF, oled->buffer_size);
		break;

	case OLED_BLACK:
		memset(oled->buffer, 0x00, oled->buffer_size);
		break;
	}
}
oled_status_t oled_SH1106Display(oled_t *oled)
{
	if ((NULL == oled->driver->WriteOneByte) || (NULL == oled->driver->WriteBurst))
	{
		return oled_ErrorCallbackPointer;
	}

  	for (int8_t i = (oled->heightOled / 8)-1; i >= 0; i--)
  	{
  		oled->driver->WriteOneByte(oled, OLED_COMMAND_ADDRESS, 0xB0 + i);		// Set row
  		oled->driver->WriteOneByte(oled, OLED_COMMAND_ADDRESS, SH1106_SETLOWCOLUMN);		// Set lower column address
  		oled->driver->WriteOneByte(oled, OLED_COMMAND_ADDRESS, OLED_SETHIGHCOLUMN); // Set higher column address

  		oled->driver->WriteBurst(oled, OLED_DATA_ADDRESS, oled->buffer+(i * oled->widthOled), oled->widthOled);
    }

	return oled_NoError;
}

oled_status_t oled_SSD1306Display(oled_t *oled)
{
	if ((NULL == oled->driver->WriteBurst) || (NULL == oled->driver->WriteBurst))
	{
		return oled_ErrorCallbackPointer;
	}
	uint8_t CommandToSend[]=
	{
			OLED_PAGEADDR,
			0,                   	// Page start address
			0xFF,                   // Page end (not really, but works here)

			OLED_COLUMNADDR,
		     0, 					// Column start address

		     oled->widthOled - 1, 	// Column end address
	};

	oled->driver->WriteBurst(oled, OLED_COMMAND_ADDRESS, CommandToSend, sizeof(CommandToSend));

	oled->driver->WriteBurst(oled, OLED_DATA_ADDRESS, oled->buffer, oled->buffer_size);

    return oled_NoError;
}

oled_status_t oled_Display(oled_t *oled)
{
	if((oled->typeOled >= 1) && (oled->typeOled <= 3)) 	//ssd from 1 to 3
	{
		return oled_SSD1306Display(oled);
	}

	if((oled->typeOled >= 4) && (oled->typeOled <= 6)) 	//sh from 4 to 6
	{
		return oled_SH1106Display(oled);
	}

	return oled_ErrorBadOledType;
}

void Oled_SetOledSize(oled_t *oled)
{
	switch (oled->typeOled) {
		case SSD1306_128_64:
			oled->heightOled = OLED_128_64_LCDHEIGHT;
			oled->widthOled = OLED_128_64_LCDWIDTH;
			break;

		case SSD1306_128_32:
			oled->heightOled = OLED_128_32_LCDHEIGHT;
			oled->widthOled = OLED_128_32_LCDWIDTH;
			break;

		case SSD1306_96_16:
			oled->heightOled = OLED_96_16_LCDHEIGHT;
			oled->widthOled = OLED_96_16_LCDWIDTH;
			break;

		case SH1106_128_64:
			oled->heightOled = OLED_128_64_LCDHEIGHT;
			oled->widthOled = OLED_128_64_LCDWIDTH;
			break;

		case SH1106_128_32:
			oled->heightOled = OLED_128_32_LCDHEIGHT;
			oled->widthOled = OLED_128_32_LCDWIDTH;
			break;

		case SH1106_96_16:
			oled->heightOled = OLED_96_16_LCDHEIGHT;
			oled->widthOled = OLED_96_16_LCDWIDTH;
			break;
	}
}

uint16_t Oled_GetBufferSize(oled_t *oled)
{
	return ((oled->heightOled) * (oled->widthOled) /8);
}

uint8_t Oled_GetMultiplexValue(oled_t *oled)
{
	uint8_t MultiplexValue;
	switch (oled->typeOled) {
		case SSD1306_128_64:
			MultiplexValue = 0x3F;
			break;

		case SSD1306_128_32:
			MultiplexValue = 0x1F;
			break;

		case SSD1306_96_16:
			MultiplexValue = 0x0F;
			break;

		case SH1106_128_64:
			MultiplexValue = 0x3F;
			break;

		case SH1106_128_32:
			MultiplexValue = 0x1F;
			break;

		case SH1106_96_16:
			MultiplexValue = 0x0F;
			break;
	}

	return MultiplexValue;
}

uint8_t Oled_GetCompinsValue(oled_t *oled)
{
	uint8_t CompinsValue;
	switch (oled->typeOled) {
		case SSD1306_128_64:
			CompinsValue = 0x12;
			break;

		case SSD1306_128_32:

		case SSD1306_96_16:
			CompinsValue = 0x2;
			break;

		case SH1106_128_64:
			CompinsValue = 0x12;
			break;

		case SH1106_128_32:

		case SH1106_96_16:
			CompinsValue = 0x2;
			break;
	}

	return CompinsValue;
}

uint8_t Oled_GetContrastValue(oled_t *oled)
{
	uint8_t ContrastValue;
	switch (oled->typeOled) {
		case SSD1306_128_64:
			ContrastValue = 0xCF;
			break;

		case SSD1306_128_32:
			ContrastValue = 0x8F;
			break;

		case SSD1306_96_16:
			ContrastValue = 0xAF;
			break;

		case SH1106_128_64:
			ContrastValue = 0xCF;
			break;

		case SH1106_128_32:
			ContrastValue = 0x8F;
			break;

		case SH1106_96_16:
			ContrastValue = 0xAF;
			break;
	}

	return ContrastValue;
}


oled_status_t oled_Init(oled_t *oled, uint8_t OledType, I2C_HandleTypeDef *OledI2cHandle, uint8_t OledAdress)
{
	oled->i2cOledHandle = OledI2cHandle;
	oled->i2cOledAdreess = OledAdress;

	if((OledType < 1)  || (OledType > 6))
	{
		return oled_ErrorBadOledType;
	}
	oled->typeOled = OledType;

	Oled_SetOledSize(oled);

	oled->buffer_size = Oled_GetBufferSize(oled);
	oled->buffer = (uint8_t*)malloc(oled->buffer_size);

	if ((NULL == oled->driver->WriteOneByte) || (NULL == oled->driver->WriteBurst))
	{
		return oled_ErrorCallbackPointer;
	}

	uint8_t CommandToSend[]=
			{
					OLED_DISPLAYOFF,

					OLED_SETDISPLAYCLOCKDIV,
					0x80,

					OLED_SETMULTIPLEX,
					Oled_GetMultiplexValue(oled),

					OLED_SETDISPLAYOFFSET,
					0x0,

					OLED_SETSTARTLINE | 0x0,

					OLED_CHARGEPUMP,
					0x14,

					OLED_MEMORYMODE,
					0x00,

					OLED_SEGREMAP | 0x1,
					OLED_COMSCANDEC,

					OLED_SETCOMPINS,
					Oled_GetCompinsValue(oled),

					OLED_SETCONTRAST,
					Oled_GetContrastValue(oled),

					OLED_SETPRECHARGE,
					0xF1,

					OLED_SETVCOMDETECT,
					0x40,

					OLED_DISPLAYALLON_RESUME,
					OLED_NORMALDISPLAY,
					OLED_DEACTIVATE_SCROLL,
					OLED_DISPLAYON
			};

	oled->driver->WriteBurst(oled, OLED_COMMAND_ADDRESS, CommandToSend, sizeof(CommandToSend)); // before 6.9 ms after: 2.238 ms

 	return oled_NoError;
}

oled_status_t Oled_DeInit(oled_t *oled, uint8_t OledType, I2C_HandleTypeDef *OledI2cHandle, uint8_t OledAdress)
{
	free(oled->buffer);
	oled->buffer = NULL;

	return oled_Init(oled, OledType, OledI2cHandle, OledAdress);


}

