/*
 * oled_i2c_interface.h
 *
 *  Created on: Nov 3, 2023
 *      Author: Jan Lukaszewicz
 */

#ifndef oled_I2C_INTERFACE_H_
#define oled_I2C_INTERFACE_H_

#include"oled_library.h"

oled_driver_t* oled_GetI2cDriver (void);

void oled_WriteOneByte(oled_t *oled,  uint8_t CommandOrDataAddress, uint8_t CommandToSend);
void oled_WriteBurst(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t *CommandToSend, uint16_t SizeDataToSend);


#endif /* oled_I2C_INTERFACE_H_ */
