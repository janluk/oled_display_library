# SH1106 SSD1306 OLED library (Tested on STM32 NUCLEO-F411RE)

* [General info](#general-info)
* [Setup](#setup)
* [additional functions](#additional-functions)
* [Author and license](#author-and-license)


# **General info** 
The library has been tested on STM32 F411RE. It works with HAL framework and It was also tested i2c data speed.

## Supported oled displays:
* SSD1306_128_64
* SH1106_128_64

* SSD1306_128_32
* SH1106_128_32

* SSD1306_96_16
* SH1106_96_16
	
## Tested i2c speed:	
* i2c 100kHz:
	
	Init: 2.6ms
	
	Date: 99.4ms = 10Hz
		
* i2c 400kHz:
	
	Init: 772us
	
	Date: 29.4ms = 34.4Hz

<details>
<summary><b>Click here to see  comparison picture from logic analyzer</b></summary>
![i2c_speed1](doc/foto/sh1106_100kHz_vs_400kHz.png "compare i2c speed 100kHz vs 400kHz")
![i2c_speed2](doc/foto/sh1106_100kHz.png "compare i2c speed 100kHz")
![i2c_speed3](doc/foto/sh1106_400kHz.png "compare i2c speed 400kHz")
</details>

# **Setup** 

## [1] Include files
```C
#include "GFX_BW.h"
#include "oled_library.h"
#include "oled_i2c_interface.h"
#include "logo.h"
#include "fonts/font_8x5.h"
```

## [2] Defines
```C
#define OLED_I2C_ADDRESS   0x3C //0x3D	// 011110+SA0+RW - 0x3C or 0x3D
```

## [3] Driver structure
```C
struct oled_driver
{
	void (*WriteOneByte)(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t ByteToSend);
	void (*WriteBurst)(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t *DataToSend, uint16_t SizeDataToSend);
};
```
</br>

<details>
<summary>Click here to see <b>interface example for i2c STM32 HAL</b></summary>


<b>oled_i2c_interface.c</b>

```C
#include "oled_i2c_interface.h"

static oled_driver_t oledDriver ={
		oled_WriteOneByte,
		oled_WriteBurst,
};

oled_driver_t* oled_GetI2cDriver(void)
{
	return &oledDriver;
}

void oled_WriteOneByte(oled_t *oled,  uint8_t CommandOrDataAddress, uint8_t CommandToSend)
{
	HAL_I2C_Mem_Write(oled->i2cOledHandle, ((oled->i2cOledAdreess)<<1), CommandOrDataAddress, 1, &CommandToSend, 1, OLED_I2C_TIMEOUT);
}

void oled_WriteBurst(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t *CommandToSend, uint16_t SizeDataToSend)
{
	HAL_I2C_Mem_Write(oled->i2cOledHandle, ((oled->i2cOledAdreess)<<1), CommandOrDataAddress, 1, CommandToSend, SizeDataToSend, OLED_I2C_TIMEOUT);
}
```
<b>oled_i2c_interface.h</b>

```c
#include"oled_library.h"

oled_driver_t* oled_GetI2cDriver (void);

void oled_WriteOneByte(oled_t *oled,  uint8_t CommandOrDataAddress, uint8_t CommandToSend);
void oled_WriteBurst(oled_t *oled, uint8_t CommandOrDataAddress, uint8_t *CommandToSend, uint16_t SizeDataToSend);
```  

</details>


## [4] Initialize structures

 Main structures:

```C
oled_t oled1;
```

## [5] Initialized library

```C
oled_LinkDriver(&oled1, oled_GetI2cDriver());

//  oled_Init(&oled1, SSD1306_128_64, &hi2c1, OLED_I2C_ADDRESS);
// 	oled_Init(&oled1, SH1106_128_64, &hi2c1, OLED_I2C_ADDRESS);
oled_Init(&oled1, SSD1306_128_32, &hi2c1, OLED_I2C_ADDRESS);
//  oled_Init(&oled1, SH1106_128_32, &hi2c1, OLED_I2C_ADDRESS);

oled_ClearBuffer(&oled1, OLED_BLACK);
oled_Display(&oled1);

GFX_SetFont(font_8x5);
```

## [6] Example main loop:
```C
	    GFX_Image(&oled1, 0, 10, logo, 128, 38, OLED_WHITE);
	    oled_Display(&oled1);

	    HAL_Delay(2000);

	    oled_ClearBuffer(&oled1, OLED_BLACK);
	    GFX_SetFontSize(4);
	    GFX_DrawString(&oled1, 5, 1, "Hareo", OLED_WHITE, 0);
	    GFX_SetFontSize(1);
	    GFX_DrawString(&oled1, 0, 40, "your hardware support", OLED_WHITE, 0);
	    oled_Display(&oled1);

	    HAL_Delay(2000);
	    oled_invertDisplay(&oled1, 1);

	    HAL_Delay(2000);
	    oled_invertDisplay(&oled1, 0);

	    HAL_Delay(2000);
	    oled_StartScrollRight(&oled1, 0x00, 0x07);

	    HAL_Delay(2000);
	    oled_StopScroll(&oled1);
	    oled_StartScrollLeft(&oled1, 0x00, 0x07);

	    HAL_Delay(2000);
	    oled_StopScroll(&oled1);
	    oled_StartScrollDiagRight(&oled1, 0x07, 0x07);

	    HAL_Delay(2000);
	    oled_StopScroll(&oled1);
	    oled_StartScrollDiagRight(&oled1, 0x00, 0x07);

	    HAL_Delay(2000);
	    oled_StopScroll(&oled1);
	    oled_StartScrollDiagLeft(&oled1, 0x00, 0x0f);

	    HAL_Delay(2000);
	    oled_StopScroll(&oled1);
	    oled_StartScrollDiagLeft(&oled1, 0x0f, 0x0f);
	    oled_StopScroll(&oled1);
	    HAL_Delay(2000);

```

# **Additional functions**

```C
GFX_DrawCircle(&oled1, 10, 10, 10, WHITE);
GFX_DrawLine(&oled1, 10, 20, 30, 40, WHITE);
GFX_DrawRectangle(&oled1, 10, 10, 5, 10, WHITE);
GFX_DrawFillRectangle(&oled1, 10, 10, 5, 10, WHITE);
GFX_DrawFillCircle(&oled1, 10, 10, 10, WHITE);
GFX_DrawRoundRectangle(&oled1, 10, 10, 10, 10, 10, WHITE);
GFX_DrawFillRoundRectangle(&oled1, 10, 10, 10, 10, 10, WHITE);
GFX_DrawTriangle(&oled1, 10, 10, 20, 20, 30, 40, WHITE);

oled_invertDisplay(&oled1, 1);
oled_StartScrollRight(&oled1, 0x00, 0x0F);
oled_StartScrollLeft(&oled1, 0x00, 0x0F);
oled_StartScrollDiagRight(&oled1, 0x00, 0x0F);
oled_StartScrollDiagLeft(&oled1, 0x00, 0x0F);
oled_StartScrollDiagLeft(&oled1, 0x0F, 0x0F); // scroll only vertical
oled_StopScroll(&oled1);
```


# **Author and license**

* Jan Łukaszewicz pldevluk@gmail.com

The MIT License (MIT)

Copyright 2022-present  <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
